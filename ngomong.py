import speech_recognition as sr
import webbrowser as wb

chrome_path = "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe"

r = sr.Recognizer()

with sr.Microphone() as source:
    print('Bicara Apa saja: ')
    audio = r.listen(source)

    try:
        text = r.recognize_google(audio)
        print('Anda Mengatakan : {}'.format(text))
        wb.get(chrome_path).open(text)
    except:
        print('Maaf tidak mengenali suara anda')